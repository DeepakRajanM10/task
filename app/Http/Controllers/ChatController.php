<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ChatController extends Controller
{
    public function showChatList()
    {
       $users = User::all()->except(auth()->id());

       return view('chat', [
           'users' => $users
       ]);
    }
}
