<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GreetController extends Controller
{
    /**
     * Show greet page
     */
    public function greet()
    {
        $user = auth()->user();

        return view('auth.greet', [
            'user' => $user
        ]);
    }
}
