@extends('welcome')
@section('content')

    <div class="col-md-12">
        <h2>Register</h2>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="col-md-12">
        <form method="POST" action="/register">
            {{ csrf_field() }}
            <div class="form-group width-50">
                <label for="name">Name:</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
    
            <div class="form-group width-50">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>
    
            <div class="form-group width-50">
                <label for="password">Password:</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
    
            <div class="form-group width-50">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
 
@endsection