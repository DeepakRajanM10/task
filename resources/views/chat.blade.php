@extends('welcome')
@section('content')

    <div class="col-md-12">
        <h2>Chat</h2>
    </div>

    <div class="col-lg-12">
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="users">User list:</label>
                    <select class="form-control" id="users">
                        @foreach($users as $user)
                            <option value="{{ $user->email }}">{{ $user->name }} ({{ $user->email }})</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-6">    
                <div class="form-group">
                    <label for="message">Messages:</label>
                    <textarea class="form-control" rows="20" id="message"></textarea>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </div>
        </div>
    </div>
 
@endsection